package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        //  Implemented Rock Paper Scissors

        boolean gameStatus = true;

        while (gameStatus == true) {
            System.out.println("Let's play round " + roundCounter);

            String userChoice = "";
            
            // checks if user input is correct and prompts new user input if wrong
            while (!rpsChoices.contains(userChoice)) {
                userChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();

                if (rpsChoices.contains(userChoice)) {
                    //System.out.println("nice");
                } else {
                    System.out.println("I don't understand " + userChoice + " Could you try again?");
                }
            }

            // random computer choice 
            Random random = new Random();
            String computerChoice = rpsChoices.get(random.nextInt(rpsChoices.size()));
            //System.out.println(computerChoice);

            // Finding the winner
            boolean winner = true;

            if (userChoice == "paper") {
                winner = computerChoice == "rock";
            } else if (userChoice == "scissors") {
                winner = computerChoice == "scissors";
            }
            else {
                winner = computerChoice == "scissors";
            }
            
            // Print the choices and winner or tie
            if (winner == true) {
                // user win
                System.out.println("Human chose " + userChoice + ", computer chose " + computerChoice + ". Human wins!");
                humanScore++;
            } else {
                if (computerChoice.equals(userChoice)){
                    // tie
                    System.out.println("Human chose " + userChoice + ", computer chose " + computerChoice + ". It's a tie!");
                } else {
                    // computer win
                    System.out.println("Human chose " + userChoice + ", computer chose " + computerChoice + ". Computer wins!");
                    computerScore++; 
                }
            }

            // print the score
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            // game status
            List<String> gameChoices = Arrays.asList("n", "y");
            String userGameChoice = "";
            while (!gameChoices.contains(userChoice)) {
                userGameChoice = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
                if (userGameChoice.equals("y")) {
                    roundCounter++;
                    break;
                } else if (userGameChoice.equals("n")) {
                    System.out.println("Bye bye :)");
                    gameStatus = false;
                    break;
                } else {
                    System.out.println("I don't understand " + userGameChoice + " Could you try again?");  
                }
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
